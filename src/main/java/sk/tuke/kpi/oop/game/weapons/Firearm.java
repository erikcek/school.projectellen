package sk.tuke.kpi.oop.game.weapons;

public abstract class Firearm {

    private int actualAmmo;
    private int maxAmmo;

    public Firearm(int actualAmmo, int maxAmmo) {
        this.actualAmmo = actualAmmo;
        this.maxAmmo = maxAmmo;
    }

    public Firearm(int ammo) {
        this.actualAmmo = ammo;
        this.maxAmmo = ammo;
    }

    public int getAmmo() {
        return this.actualAmmo;
    }

    public void reload(int newAmmo) {
        this.actualAmmo = Math.min(this.actualAmmo + newAmmo, this.maxAmmo);
    }

    public Fireable fire() {
        if (this.actualAmmo > 0) {
            this.actualAmmo -= 1;
            return this.createBullet();
        } else {
            return null;
        }
    };
    protected abstract Fireable createBullet();

}
