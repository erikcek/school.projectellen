package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.characters.Alive;
import sk.tuke.kpi.oop.game.weapons.Fireable;

public class RemoveBullet<T extends Fireable> extends AbstractAction<T> {

    private Alive b;

    public RemoveBullet(Alive b) {
        this.b = b;
    }

    @Override
    public void execute(float deltaTime) {
        Fireable actor = this.getActor();
        if (actor == null || this.b == null) return;
        Scene s = actor.getScene();
        if (s == null) return;
        if (actor.intersects(this.b) ) {
            s.removeActor(actor);
            this.b.getHealth().drain(10);
            this.setDone(true);
        }
    }
}
