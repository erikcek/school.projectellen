package sk.tuke.kpi.oop.game;
import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.When;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.Player;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class Teleport extends AbstractActor {

    private Teleport destination;
    private boolean blocked;

    public Teleport(Teleport destination) {
        this.setDestination(destination);
        this.blocked = false;
        Animation normal = new Animation("sprites/lift.png", 48, 48);
        setAnimation(normal);
    }

    public Teleport getDestination() {
        return destination;
    }

    public void setDestination(Teleport destination) {
        if (this != destination)
        this.destination = destination;
    }

    public void teleportPlayer(Player player) {
        if (player != null) {
            this.setBlocked(true);
            player.setPosition(this.getPosX() + (this.getWidth() / 2) - (player.getWidth() / 2),
                this.getPosY() + (this.getHeight() / 2) - (player.getHeight() / 2));
        }
    }

    private boolean intersectsPlayer(Player player) {
        Rectangle2D.Float position = new Rectangle2D.Float(this.getPosX(), this.getPosY(), this.getWidth(), this.getHeight());
        Point2D.Float playerPosition = new Point2D.Float(player.getPosX() + player.getWidth() / 2,
            player.getPosY() + player.getHeight() / 2);
        return position.contains(playerPosition);

    }

    private void findAndTeleportPlayer() {
        if (this.destination == null) {
            return;
        }
        Scene s = this.getScene();
        if (s == null) {
            return;
        }
        Actor player = this.getScene().getFirstActorByName("Player");

        this.destination.teleportPlayer((Player) player);
    }

    private void removeTeleportBlock() {
        this.setBlocked(false);
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        Actor player = scene.getFirstActorByName("Player");
        if (player == null) { return; }
        new Loop<>(new When<>(() -> {
                return (!this.blocked && intersectsPlayer((Player) player));
            },
            new Invoke<>(this::findAndTeleportPlayer))).scheduleFor(this);
        new Loop<>(new When<>(() -> {
            return (this.blocked && !intersectsPlayer((Player) player));
        },
            new Invoke<>(this::removeTeleportBlock))).scheduleFor(this);
    }
}
