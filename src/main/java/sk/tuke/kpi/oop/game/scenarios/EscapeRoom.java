package sk.tuke.kpi.oop.game.scenarios;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sk.tuke.kpi.gamelib.*;
import sk.tuke.kpi.oop.game.behaviours.Chasing;
import sk.tuke.kpi.oop.game.behaviours.Observing;
import sk.tuke.kpi.oop.game.behaviours.RandomlyMoving;
import sk.tuke.kpi.oop.game.characters.*;
import sk.tuke.kpi.oop.game.controllers.KeeperController;
import sk.tuke.kpi.oop.game.controllers.MovableController;
import sk.tuke.kpi.oop.game.controllers.ShooterController;
import sk.tuke.kpi.oop.game.items.Ammo;
import sk.tuke.kpi.oop.game.items.Energy;
import sk.tuke.kpi.oop.game.openables.Door;

public class EscapeRoom implements SceneListener {

    public static class Factory implements ActorFactory {
        @Override
        public @Nullable Actor create(@Nullable String type, @Nullable String name) {
            switch (name) {
                case "ellen":
                    return new Ripley();
                case "ammo":
                    return new Ammo();
                case "energy":
                    return new Energy();
                case "alien":
                   return this.createAlliens(name, type);
                case "alien mother":
//                    return new AlienMother(new Behaviour<AlienMother>() {
//                        @Override
//                        public void setUp(AlienMother actor) {
//                            System.out.println("settedUp");
//                        }
//                    });
                    return new HandyMonster(new RandomlyMoving());
                default:
                    break;
            }
            switch (type) {
                case "vertical":
                    return new Door(name, Door.Orientation.VERTICAL);
                case "horizontal":
                    return new Door(name, Door.Orientation.HORIZONTAL);
                default:
                    return null;
            }
        }

        public  Actor createAlliens(String name, String type) {
            if (type.equals("running")) {
                return new Alien(100,new RandomlyMoving());
            }
            else if (type.equals("waiting1")) {
                return new Alien(100,
                    new Observing<>(
                        Door.DOOR_OPENED,
                        actor -> actor.getName().equals("front door"),
                        new Chasing()
                    )
                );
            }
            else {
                return new Alien(100,
                    new Observing<>(
                        Door.DOOR_OPENED,
                        actor -> actor.getName().equals("back door"),
                        new Chasing()
                    )
                );
            }
        }
    }


//    @Override
//    public void sceneCreated(@NotNull Scene scene) {
////        Bullet b = new Bullet();
////        scene.addActor(b, 100, 200);
////        new Move<>(Direction.NORTH, 9999).scheduleFor(b);
//        Random random = new Random();
////        scene.getMessageBus().subscribe(World.ACTOR_ADDED_TOPIC, item -> {
////            if (item instanceof Alien) {
////                int r  = random.nextInt(Direction.values().length);
////                Direction d = Direction.values()[r];
////                Alien a = (Alien) item;
////                new While<Alien>(() -> true,  new ActionSequence<>(
////                    new Wait<>(3),
////                    new Move<>(d, random.nextInt(3))
////                    )).scheduleFor(a);
////                    return new Move<>(Direction.values()[new Random().nextInt(Direction.values().length)], new Random().nextInt(3))
//
//
////                ).scheduleFor(a);
////            }
////        });
//    }

        @Override
    public void sceneInitialized(@NotNull Scene scene) {
            Ripley r = (Ripley) scene.getFirstActorByName("Ellen");
            if (r == null) {
                return;
            }
            MovableController<Ripley> mc = new MovableController<Ripley>(r);
            ShooterController<Ripley> sc = new ShooterController<>(r);
            KeeperController<Ripley> kc = new KeeperController<>(r);
            scene.getInput().registerListener(sc);
            scene.getInput().registerListener(kc);
            scene.getInput().registerListener(mc);

            scene.getMessageBus().subscribe(Door.DOOR_OPENED, item -> {return;});

    }

    @Override
    public void sceneUpdating(@NotNull Scene scene) {
        Ripley r = (Ripley) scene.getFirstActorByName("Ellen");
        if (r == null) {
            return;
        }

        r.showRiplayState(scene);
        scene.follow(r);
    }
}
