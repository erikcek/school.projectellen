package sk.tuke.kpi.oop.game.characters;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.ActionSequence;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.Wait;
import sk.tuke.kpi.gamelib.actions.When;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.messages.Topic;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.behaviours.Behaviour;


public class Alien extends AbstractActor implements Movable, Enemy, Alive {

    public static final Topic<Alien> ENEMY_DIED = Topic.create("enemy died", Alien.class);

    private int speed;
    private Health health;
//    private Instant time;
    private Behaviour<? super Alien> behaviour;
    private Animation normalAnimation;

    public Alien() {
        this.normalAnimation = new Animation("sprites/alien.png", 32, 32, 0.1f);
        this.setAnimation(normalAnimation);
        this.speed = 1;
        this.health = new Health(100);
//        this.time = Instant.now();
        this.behaviour = null;
        this.health.onExhaustion(this::die);
    }

    public Alien(int healthValue, Behaviour<? super Alien> behaviour) {
        this.normalAnimation = new Animation("sprites/alien.png", 32, 32, 0.1f);
        this.setAnimation(normalAnimation);
        this.speed = 1;
        this.health = new Health(healthValue);
//        this.time = Instant.now();
        this.behaviour = behaviour;
        this.health.onExhaustion(this::die);
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        scene.getActors().stream()
            .filter(item -> item instanceof Alive)
            .filter(item -> !(item instanceof Enemy))
            .forEach(item -> this.decreaseHealthAction(item));
        if (this.behaviour != null) {
            this.behaviour.setUp(this);
        }
    }

    private void decreaseHealthAction(Actor actor) {

        new Loop<>(
            new When<>(() -> this.intersects(actor),
                new ActionSequence<>(
                    new Invoke<Actor>(() -> {
                        Alive aliveActor = (Alive)actor;
                        aliveActor.getHealth().drain(5);
                    }),
                    new Wait<>(1)
                )
            )
        ).scheduleFor(this);
    }

//    private void decreaseHealth(Alive actor) {
//        if (Duration.between(this.time, Instant.now()).toMillis() > 1000) {
//            actor.getHealth().drain(5);
//            this.time = Instant.now();
//        }
//    }

    private void die() {
        Scene s = this.getScene();
        if (s == null) return;
        s.removeActor(this);
        this.dieMessage();
    }

    public void startedMoving(Direction direction) {
        float angle = direction.getAngle();
        this.normalAnimation.setRotation(angle);
        this.normalAnimation.play();
        this.setAnimation(this.normalAnimation);

    }

    private void dieMessage() {
        Scene s = this.getScene();
        if (s != null) {
            s.getMessageBus().publish(Alien.ENEMY_DIED, this);
        }
    }

    @Override
    public Health getHealth() {
        return this.health;
    }
}
