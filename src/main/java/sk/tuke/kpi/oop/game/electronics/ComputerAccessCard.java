package sk.tuke.kpi.oop.game.electronics;

import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.items.Collectible;
import sk.tuke.kpi.oop.game.items.Usable;

public class ComputerAccessCard extends AbstractActor implements Usable<HackableComputer>, Collectible {

    public ComputerAccessCard() {
        Animation a = new Animation("sprites/key.png");
        this.setAnimation(a);
    }

    @Override
    public void useWith(HackableComputer actor) {
        if (actor != null) {
            actor.hack();
        }
    }

    @Override
    public Class<HackableComputer> getUsingActorClass() {
        return HackableComputer.class;
    }
}
