package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class ChainBomb extends TimeBomb {

    public ChainBomb(float time) {
        super(time);
    }

    @Override
    protected void explode() {
        super.explode();

        Scene s = this.getScene();
        int radius = 51;
        Ellipse2D.Float f = new Ellipse2D.Float(this.getPosX() - radius,
            this.getPosY() - radius, 2* radius, 2* radius);
        List<Actor> actors = s.getActors();
        for (Actor a: actors) {
            if (a instanceof ChainBomb && !((ChainBomb) a).isActivated()) {
                Rectangle2D.Float nextChainBomb = new Rectangle2D.Float(a.getPosX(), a.getPosY(), 16, 16);
                if (f.intersects(nextChainBomb)) {
                    ((ChainBomb) a).activate();
                }
            }

        }
    }
}
