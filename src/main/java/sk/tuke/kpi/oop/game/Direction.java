package sk.tuke.kpi.oop.game;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public enum Direction {
    NORTH(0,1),
    EAST(1, 0),
    SOUTH(0, -1),
    WEST(-1, 0),
    NORTHEAST(1,1),
    NORTHWEST(-1,1),
    SOUTHWEST(-1,-1),
    SOUTHEAST(1,-1),
    NONE(0,0);

    private final int dx;
    private final int dy;

    private static final Map<Direction, Float> dirToAngle = new HashMap<>();

    static {
        for (Direction d : values()) {
           switch (d) {
               case NORTH:
                   dirToAngle.put(NORTH, 0f);
                   break;
               case NORTHWEST:
                   dirToAngle.put(NORTHWEST, 45f);
                   break;
               case WEST:
                   dirToAngle.put(WEST, 90f);
                   break;
               case SOUTHWEST:
                   dirToAngle.put(SOUTHWEST, 135f);
                   break;
               case SOUTH:
                   dirToAngle.put(SOUTH, 180f);
                   break;
               case SOUTHEAST:
                   dirToAngle.put(SOUTHEAST, 225f);
                   break;
               case EAST:
                   dirToAngle.put(EAST, 270f);
                   break;
               case NORTHEAST:
                   dirToAngle.put(NORTHEAST, 315f);
                   break;
               case NONE:
                   dirToAngle.put(NONE, 0f);
                   break;
               default:
                   dirToAngle.put(NONE, 0f);
                   break;
           }
        }
    }

    Direction(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }

    public float getAngle() {
        return dirToAngle.get(this);
    }

    public Direction combine(Direction other) {
        int newX = this.dx + other.dx;
        if (newX > 1) {
            newX = 1;
        }
        if (newX < -1) {
            newX = -1;
        }

        int newY = this.dy + other.dy;
        if (newY > 1) {
            newY = 1;
        }
        if (newY < -1) {
            newY = -1;
        }

        final int fx = newX;
        final int fy = newY;

        return Arrays.stream(Direction.values()).filter(d -> d.getDx() == fx && d.getDy() == fy).findFirst().orElse(NONE);
    }

    public static Direction fromAngle(float angle) {
        if (angle == 0f) {
            return Direction.NORTH;
        }
        else if (angle == 45f) {
            return Direction.NORTHWEST;
        }
        else if (angle == 90f) {
            return Direction.WEST;
        }
        else if (angle == 135f) {
            return Direction.SOUTHWEST;
        }
        else if (angle == 180f) {
            return Direction.SOUTH;
        }
        else if (angle == 225f) {
            return Direction.SOUTHEAST;
        }
        else if (angle == 270f) {
            return Direction.EAST;
        }
        else if (angle == 315f) {
            return Direction.NORTHEAST;
        }
        else {
            return Direction.NONE;
        }

    }


    public static Direction getRandomDirection() {
        Random r = new Random();
        int randomInt = r.nextInt(Direction.values().length - 1);
        return Direction.values()[randomInt];
    }
}

