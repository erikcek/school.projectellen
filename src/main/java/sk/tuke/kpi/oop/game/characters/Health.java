package sk.tuke.kpi.oop.game.characters;

import java.util.HashSet;
import java.util.Set;

public class Health {

    private int maxHealth;
    private int actualHealth;

    private Set<ExhaustionEffect> ef;
    private boolean firedCallbasks;

    public Health(int actualHealth, int maxHealth) {
        this.actualHealth = actualHealth;
        this.maxHealth = maxHealth;
        this.ef = new HashSet<ExhaustionEffect>();
        this.firedCallbasks = false;
    }

    public Health(int health) {
        this.actualHealth = health;
        this.maxHealth = health;
        this.ef = new HashSet<ExhaustionEffect>();
        this.firedCallbasks = false;
    }

    public int getValue() {
        return this.actualHealth;
    }

    public void refill(int amount) {
        this.actualHealth = Math.min(this.maxHealth, this.actualHealth + amount);
    }

    public void restore() {
        this.actualHealth = this.maxHealth;
    }

    public void drain(int amount) {
        this.actualHealth = Math.max(0, this.actualHealth - amount);
        if (this.actualHealth == 0 && !this.firedCallbasks) {
            fireCallbacks();
        }
    }

    public void exhaust() {
        this.actualHealth = 0;
        if (!this.firedCallbasks) {
            fireCallbacks();
        }
    }

    @FunctionalInterface
    public interface ExhaustionEffect {
        void apply();
    }

    public void onExhaustion(ExhaustionEffect effect) {
        this.ef.add(effect);
    };

    private void fireCallbacks() {
        this.firedCallbasks = true;
        this.ef.stream().forEach(item -> {
            item.apply();
        });
    }
}
