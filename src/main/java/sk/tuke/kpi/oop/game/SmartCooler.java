package sk.tuke.kpi.oop.game;

public class SmartCooler extends Cooler {
    public SmartCooler(Reactor reactor) {
        super(reactor);
    }

    @Override
    public void coolReactor() {
        super.coolReactor();
        Reactor r = this.getReactor();

        if (r == null) {
            return;
        }

        if (r.getTemperature() > 2500) {
            this.turnOn();
        }
        if (r.getTemperature() <= 1500) {
            this.turnOff();
        }
    }
}
