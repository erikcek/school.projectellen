package sk.tuke.kpi.oop.game.weapons;

import sk.tuke.kpi.gamelib.Actor;

public interface BulletBehavior<A extends Actor> {

    public void setUp(A actor);
}
