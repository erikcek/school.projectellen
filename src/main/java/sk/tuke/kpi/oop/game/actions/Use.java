package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Disposable;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.items.Usable;

public class Use<A extends Actor> extends AbstractAction<A> {

    private Usable<A> usableActor;

    public <T extends Usable<A>> Use(T actor) {
        this.usableActor = actor;
    }

    @Override
    public void execute(float deltaTime) {
        if (this.usableActor != null) {
//            Actor a = this.getActor();
//            Actor b = super.getActor();
            this.usableActor.useWith(this.getActor());
        }
        this.setDone(true);
    }

    public Disposable scheduleForIntersectingWith(Actor mediatingActor) {
        Scene scene = mediatingActor.getScene();
        if (scene == null) {
            return null;
        }
        Class<A> usingActorClass = this.usableActor.getUsingActorClass();
        return scene.getActors().stream()
            .filter(mediatingActor::intersects)
            .filter(usingActorClass::isInstance)
            .map(usingActorClass::cast)
            .findFirst()
            .map(this::scheduleFor)
            .orElse(null);
    }

}
