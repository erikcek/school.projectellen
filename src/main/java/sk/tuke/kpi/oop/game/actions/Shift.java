package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.Keeper;

public class Shift<T extends Keeper> extends AbstractAction<T> {

    @Override
    public void execute(float deltaTime) {
        Keeper keeper = this.getActor();
        if (keeper == null) {
            this.setDone(true);
            return;
        }
        Scene s = keeper.getScene();
        if (s == null) {
            this.setDone(true);
            return;
        }
        keeper.getBackpack().shift();
        this.setDone(true);
    }
}
