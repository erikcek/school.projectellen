package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Disposable;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.ActionSequence;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.Wait;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import java.util.Random;

public class DefectiveLight extends Light implements Repairable {

    private Disposable d;
    private boolean repaired;

    public DefectiveLight() {
        super();
        this.repaired = false;
    }

    private void changeLight() {
            this.repaired = false;
            Random r = new Random();
            if (r.nextInt((20 - 1) + 1) + 1 == 1) {
                super.toggle();
            }
    }

    @Override
    public boolean repair() {
        if(this.d == null || this.repaired) {
            return false;
        }

        this.repaired = true;
        this.d.dispose();
        this.turnOn();
        this.d = new ActionSequence<>(new Wait<>(10),new Loop<>(new Invoke<Actor>(this::changeLight))).scheduleFor(this);
        return true;
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        this.d = new Loop<>(new Invoke<Actor>(this::changeLight)).scheduleFor(this);
    }
}
