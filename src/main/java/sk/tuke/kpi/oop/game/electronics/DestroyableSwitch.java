package sk.tuke.kpi.oop.game.electronics;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.messages.Topic;

public class DestroyableSwitch extends AbstractActor implements Destroyable {

    public static final Topic<DestroyableSwitch> SWITCH_DESTROYED = Topic.create("switch destroyed", DestroyableSwitch.class);

    private Animation normalAniamtion;

    public DestroyableSwitch() {
        this.normalAniamtion = new Animation("sprites/destroyable_switch.png", 16, 16);
        this.normalAniamtion.stop();
        this.setAnimation(this.normalAniamtion);
    }

    public void destroy() {
        this.normalAniamtion.setPlayMode(Animation.PlayMode.ONCE);
        this.normalAniamtion.play();
        Scene s = this.getScene();
        if (s != null) {
            s.getMessageBus().publish(DestroyableSwitch.SWITCH_DESTROYED, this);
        }
    }


}
