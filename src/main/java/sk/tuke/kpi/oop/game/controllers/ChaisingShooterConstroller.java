package sk.tuke.kpi.oop.game.controllers;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Input;
import sk.tuke.kpi.gamelib.KeyboardListener;
import sk.tuke.kpi.oop.game.actions.ChasingFire;
import sk.tuke.kpi.oop.game.actions.Fire;
import sk.tuke.kpi.oop.game.characters.Armed;


public class ChaisingShooterConstroller<T extends Armed> implements KeyboardListener {

    private T actor;

    public ChaisingShooterConstroller(T actor) {
        this.actor = actor;
    }

    @Override
    public void keyPressed(@NotNull Input.Key key) {
        if (key == Input.Key.SPACE) {
            new Fire<>().scheduleFor(actor);
        }
        if (key == Input.Key.C) {
            new ChasingFire<>().scheduleFor(actor);
        }
    }
}
