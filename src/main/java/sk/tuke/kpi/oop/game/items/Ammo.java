package sk.tuke.kpi.oop.game.items;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.characters.Armed;

public class Ammo extends AbstractActor implements Usable<Armed> {

    public Ammo() {
        Animation normalAnimation = new Animation("sprites/ammo.png", 16, 16);
        this.setAnimation(normalAnimation);
    }

    @Override
    public void useWith(Armed actor) {
        if (actor != null) {
            actor.getFirearm().reload(50);
            Scene s = this.getScene();
            if (s != null) {
                s.removeActor(this);
            }
        }
    }

    @Override
    public Class<Armed> getUsingActorClass() {
        return Armed.class;
    }
}
