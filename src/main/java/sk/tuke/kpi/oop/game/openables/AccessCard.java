package sk.tuke.kpi.oop.game.openables;

import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.items.Usable;
import sk.tuke.kpi.oop.game.items.Collectible;

public class AccessCard extends AbstractActor implements Usable<LockedDoor>, Collectible {


    public AccessCard() {
        Animation normalAnimation = new Animation("sprites/key.png");
        this.setAnimation(normalAnimation);
    }
    @Override
    public void useWith(LockedDoor actor) {
        if (actor != null && actor.isLocked()) {
            actor.unlock();
        }
    }

    @Override
    public Class<LockedDoor> getUsingActorClass() {
        return LockedDoor.class;
    }


}
