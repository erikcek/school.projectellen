package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;

public class Cooler extends AbstractActor implements Switchable {
    private Reactor reactor;
    private boolean isOn;
    private Animation normalAnimation;
    private Animation turnOffAnimation;

    public Cooler(Reactor reactor) {
        this.reactor = reactor;
        this.isOn = true;
        this.normalAnimation = new Animation("sprites/fan.png", 32, 32, 0.2f);
        this.turnOffAnimation = new Animation("sprites/fan.png", 32,32, 0);
        setAnimation(this.normalAnimation);
    }

    public void turnOn() {
        this.isOn = true;
        updateAnimation();
    }

    public void turnOff() {
        this.isOn = false;
        updateAnimation();
    }

    protected Reactor getReactor() {
        return reactor;
    }

    public boolean isOn() {
        return isOn;
    }

    private void updateAnimation() {
        if (this.isOn) {
            setAnimation(this.normalAnimation);
        }
        else {
            setAnimation(this.turnOffAnimation);
        }
    }

    public void coolReactor() {
        if (this.isOn && this.reactor != null) {
            this.reactor.decreaseTemperature( 1);
        }
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        new Loop<>(new Invoke<>(this::coolReactor)).scheduleFor(this);
    }
}
