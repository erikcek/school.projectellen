package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.*;
//import sk.tuke.kpi.oop.game.scenarios.EscapeRoom;
import sk.tuke.kpi.oop.game.scenarios.MyGame;
//import sk.tuke.kpi.oop.game.scenarios.FirstSteps;
//import sk.tuke.kpi.oop.game.scenarios.MissionImpossible;

public class Main {

    public static void main(String[] args) {
        WindowSetup windowSetup = new WindowSetup("Project Ellen", 800, 600);
        Game game = new GameApplication(windowSetup);

//        Scene scene = new World("world", "maps/mission-impossible.tmx", new MissionImpossible.Factory());
//        Scene scene = new World("world", "maps/EscapeRoom/escape-room.tmx", new EscapeRoom.Factory());
        Scene scene = new World("world", "maps/MyGame/MyGame.tmx", new MyGame.Factory());
        game.addScene(scene);

        game.getInput().onKeyPressed(Input.Key.ESCAPE, () -> game.stop());

//        FirstSteps f = new FirstSteps();
//        MissionImpossible f = new MissionImpossible();
//        EscapeRoom f = new EscapeRoom();
        MyGame f = new MyGame();
        scene.addListener(f);
        game.start();
    }
}
