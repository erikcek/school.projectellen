package sk.tuke.kpi.oop.game.electronics;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.messages.Topic;

public class DeskComputer extends HackableComputer {

    public static final Topic<DeskComputer> DESK_HACKED = Topic.create("desk hacked", DeskComputer.class);
    private Network network;

    public DeskComputer() {
        Animation animation = new Animation("sprites/desk.png");
        this.setAnimation(animation);
        this.network = Network.getInstance();
    }

}
