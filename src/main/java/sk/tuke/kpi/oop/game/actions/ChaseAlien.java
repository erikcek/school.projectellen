package sk.tuke.kpi.oop.game.actions;

//import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.Direction;
//import sk.tuke.kpi.oop.game.characters.Alive;
//import sk.tuke.kpi.oop.game.characters.Armed;
import sk.tuke.kpi.oop.game.characters.Enemy;
import sk.tuke.kpi.oop.game.weapons.Fireable;

public class ChaseAlien<T extends Fireable> extends AbstractAction<T> {
    @Override
    public void execute(float deltaTime) {
        Fireable actor = this.getActor();
        if (actor == null) {
            this.setDone(true);
            return;
        }
        int speed = actor.getSpeed();
        Scene s = actor.getScene();
        if (s == null) {
            this.setDone(true);
            return;
        }
        if (s.getMap().intersectsWithWall(actor)) {
            actor.collidedWithWall();
            this.setDone(true);
        }
        Enemy enemy = (Enemy) s.getActors().stream()
            .filter(item -> item instanceof Enemy)
            .sorted((item1, item2) -> {
                double dif = this.calculateDistance(item1.getPosX(), item1.getPosY(), actor.getPosX(), actor.getPosY()) - this.calculateDistance(item2.getPosX(), item2.getPosY(), actor.getPosX(), actor.getPosY());
                if (dif == 0) return 0;
                else if (dif > 0) return 1;
                else return -1;
            })
            .findFirst()
            .orElse(null);
        if (enemy == null) {
            new Move<>(Direction.getRandomDirection(), 9999).scheduleFor(actor);
            this.setDone(true);
            return;
        }


        actor.setPosition(actor.getPosX() + speed*getPosition(enemy.getPosX(), actor.getPosX()), actor.getPosY()+ speed*getPosition(enemy.getPosY(), actor.getPosY()));

    }

    int getPosition(int targetX, int actualX) {
        if (targetX > actualX) {
            return 1;
        }
        else {
            return -1;
        }
    }

    double calculateDistance(int X1, int Y1, int X2, int Y2) {
        return Math.sqrt(Math.pow((double)(X1 - X2), 2) + Math.pow((double)(Y1 - Y2), 2));
    }
}
