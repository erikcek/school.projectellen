package sk.tuke.kpi.oop.game.characters;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.GameApplication;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.messages.Topic;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Keeper;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.items.Backpack;
import sk.tuke.kpi.oop.game.weapons.Firearm;
import sk.tuke.kpi.oop.game.weapons.Gun;


public class Ripley extends AbstractActor implements Movable, Keeper, Alive, Armed {

    public static final Topic<Ripley> RIPLEY_DIED = Topic.create("riepley died", Ripley.class);

    private Animation normalAnimation;
    private int speed;
    private Backpack bakcpack;
    private Health health;
    private Firearm gun;

    public Ripley() {
        super("Ellen");
        this.normalAnimation = new Animation("sprites/player.png",
            32, 32, 0.2f, Animation.PlayMode.LOOP_PINGPONG);
        this.setAnimation(this.normalAnimation);
        this.speed = 2;
        this.bakcpack = new Backpack("Ripley's backpack", 10);
        this.health = new Health(100);
        this.health.onExhaustion(this::die);
        this.setFirearm(new Gun(100, 100));
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public void startedMoving(Direction direction) {
        float angle = direction.getAngle();
        this.normalAnimation.setRotation(angle);
        this.normalAnimation.play();
        this.setAnimation(this.normalAnimation);

    }

    @Override
    public void stoppedMoving() {
        this.normalAnimation.pause();
        this.setAnimation(this.normalAnimation);
    }

    public boolean setEnergy(int energy) {
        if (energy == 100 && this.health.getValue() == 100) {
            return false;
        }
        if (energy > 100) {
            this.health.restore();
            return true;
        }
        if (energy <= 0) {
            this.health.exhaust();
            this.die();
            return true;
        }
        this.health = new Health(energy, 100);
        return true;
    }

    public int getEnergy() {
        return this.health.getValue();
    }

    public void setAmmo(int ammo) {
        this.getFirearm().reload(ammo);
    }

    public int getAmmo() {
        return this.getFirearm().getAmmo();
    }

    public boolean increaseAmmo(int ammo) {
        if (this.getAmmo() >= 500) {
            return false;
        }
        this.setAmmo(this.getAmmo() + ammo);
        return true;
    }

    @Override
    public Backpack getBackpack() {
        return this.bakcpack;
    }

    public void showRiplayState(@NotNull Scene scene) {
        int windowHeight = scene.getGame().getWindowSetup().getHeight();
        int yTextPos = windowHeight - GameApplication.STATUS_LINE_OFFSET;
        scene.getGame().getOverlay().drawText(String.valueOf(
            this.getEnergy()), 110, yTextPos);
        scene.getGame().getOverlay().drawText(String.valueOf(this.getAmmo()), 190, yTextPos);
        scene.getGame().pushActorContainer(this.getBackpack());
    }

    private void die() {
        Animation a = new Animation("sprites/player_die.png", 32, 32,
            0.1f, Animation.PlayMode.ONCE);
        this.setAnimation(a);
        this.dieMessage();
    }

    private void dieMessage() {
        Scene s = this.getScene();
        if (s != null) {
            s.getMessageBus().publish(Ripley.RIPLEY_DIED, this);
        }
    }

    @Override
    public Health getHealth() {
        return health;
    }

    @Override
    public Firearm getFirearm() {
        return this.gun;
    }

    @Override
    public void setFirearm(Firearm weapon) {
        this.gun = weapon;
    }
}
