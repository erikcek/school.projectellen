package sk.tuke.kpi.oop.game.weapons;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.characters.Alive;


public class Bullet extends AbstractActor implements Fireable {

    private int speed;
    private Animation normalAnimation;
    public Bullet() {
        this.speed = 6;
        this.normalAnimation = new Animation("sprites/bullet.png", 16, 16);
        this.setAnimation(this.normalAnimation);
    }

    @Override
    public void startedMoving(Direction direction) {
        this.normalAnimation.setRotation(direction.getAngle());
    }

    @Override
    public void collidedWithWall() {
        Scene s = this.getScene();
        if (s != null) {
            s.removeActor(this);
        }
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
//        scene.getActors().stream()
//            .filter(item -> item instanceof Alive)
//            .forEach(item -> {
//                new RemoveBullet<Bullet>((Alive)item).scheduleFor(this);
////                new Loop<Bullet>(new Invoke<Bullet>(() -> {
////                    if (item.intersects(this)) {
////                        ((Alive)item).getHealth().drain(10);
////                        scene.removeActor(this);
////                    }
////                })).scheduleFor(this);
//            });
        new Loop<>(new Invoke<Actor>(() -> this.intesctsWithActor())).scheduleFor(this);
    }

    private void intesctsWithActor() {
        Scene scene = this.getScene();
        if (scene == null) return;
        Actor a = scene.getActors().stream()
            .filter(item -> item.intersects(this))
            .filter(item -> item instanceof Alive)
            .findFirst()
            .orElse(null);
        if (a == null) return;
        ((Alive)a).getHealth().drain(10);
        scene.removeActor(this);
    }

    @Override
    public int getSpeed() {
        return speed;
    }
}
