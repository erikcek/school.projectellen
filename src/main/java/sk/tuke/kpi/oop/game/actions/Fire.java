package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.characters.Armed;
import sk.tuke.kpi.oop.game.weapons.Fireable;
import sk.tuke.kpi.oop.game.weapons.Firearm;

public class Fire<T extends Armed> extends AbstractAction<T> {

    @Override
    public void execute(float deltaTime) {
            Armed a = this.getActor();
            if (a == null) {
                this.setDone(true);
                return;
            }
            Firearm f = a.getFirearm();
            if (f == null) {
                this.setDone(true);
                return;
            }
            Fireable b = f.fire();

            Scene s = a.getScene();
            if (s == null || b == null) {
                this.setDone(true);
                return;
            }
            float rotation = this.getActor().getAnimation().getRotation();
            Direction d = Direction.fromAngle(rotation);
            b.getAnimation().setRotation(rotation);
               s.addActor(b, a.getPosX() + a.getWidth() / 2 - b.getWidth() / 2 + (d.getDx() * (a.getWidth() / 2 + 5)),
                a.getPosY() + a.getHeight() / 2 - b.getHeight() / 2 + d.getDy() * (a.getHeight() / 2 + 5));
            new Move<>(d, 99999).scheduleFor(b);
//            new ChaseAlien<>().scheduleFor(b);
            this.setDone(true);

    }

}
