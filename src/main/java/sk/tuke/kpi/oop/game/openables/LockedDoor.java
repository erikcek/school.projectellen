package sk.tuke.kpi.oop.game.openables;

import sk.tuke.kpi.gamelib.Actor;

public class LockedDoor extends Door {

    private boolean isLocked;

    public LockedDoor() {
        super();
        this.isLocked = true;
    }

    public LockedDoor(Door.Orientation o) {
        super(o);
        this.isLocked = true;
    }

    public LockedDoor(String name, Door.Orientation o) {
        super(name, o);
        this.isLocked = true;
    }

    public void lock() {
        this.isLocked = true;
        this.close();
    }
    public void unlock() {
        this.isLocked = false;
        this.open();
    }

    @Override
    public void useWith(Actor actor) {
        if (!this.isLocked) {
            super.useWith(actor);
        }
    }

    public boolean isLocked() {
        return isLocked;
    }
}
