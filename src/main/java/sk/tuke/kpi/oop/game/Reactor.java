package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.actions.PerpetualReactorHeating;

import java.util.HashSet;
import java.util.Set;

public class Reactor extends AbstractActor implements Switchable, Repairable {

    private int temperature;
    private int damage;
    private Animation normalAnimation;
    private Animation dangerAnimation;
    private Animation brokenAnimation;
    private Animation turnedOffAnimation;
    private Animation extinguishedAnimation;
    private boolean isOn;

    private Set<EnergyConsumer> devices;

    public Reactor() {
        this.temperature = 0;
        this.damage = 0;
        this.isOn = false;
        this.devices = new HashSet<>();
        this.normalAnimation = new Animation("sprites/reactor_on.png", 80, 80, 0.1f, Animation.PlayMode.LOOP_PINGPONG);
        this.dangerAnimation = new Animation("sprites/reactor_hot.png", 80, 80, 0.05f, Animation.PlayMode.LOOP_PINGPONG);
        this.brokenAnimation = new Animation("sprites/reactor_broken.png", 80, 80, 0.1f, Animation.PlayMode.LOOP_PINGPONG);
        this.turnedOffAnimation = new Animation("sprites/reactor.png", 80, 80);
        this.extinguishedAnimation = new Animation("sprites/reactor_extinguished.png", 80, 80 );
        setAnimation(this.turnedOffAnimation);
     //   this.normalAnimation.setFrameDuration(0.1f);
    }

    public int getTemperature() {
        return temperature;
    }

    public int getDamage() {
        return damage;
    }

    public void setTemperature(int temperature) {
        if (this.isOn && temperature >= 0) {
            this.temperature = temperature;
        }
    }

    public void setDamage(int damage) {
        if (this.isOn && damage >= 0) {
            this.damage = damage;
        }
    }

    public void increaseTemperature(int increment) {
        if (!this.isOn || increment <= 0 || damage >= 100) {
            return;
        }
        int newTemp = this.temperature;
        int newDamage = this.damage;

        if (newDamage >= 33 && newDamage <= 66) {
            newTemp += Math.ceil(increment*1.5);
        }
        else if (newDamage > 66) {
            newTemp += increment*2;
        }
        else {
            newTemp += increment;
        }

        if (newTemp > 2000) {
            newDamage = Math.min(Math.round(100 * (newTemp - 2000) / 4000), 100);
        }
        setTemperature(newTemp);
        updateAnimation();
        setDamage(newDamage);
        if (newTemp >= 6000) {
            this.turnOff();
        }
    }

    public void decreaseTemperature(int decrement) {
        if (!this.isOn || decrement < 0 || this.damage >= 100) {
            return;
        }
        int newTemp = this.temperature;

        if (this.damage >= 50) {
            newTemp -= (decrement / 2);
        }
        else {
            newTemp -= decrement;
        }
        setTemperature(newTemp);
        updateAnimation();
    }

    private void updateAnimation() {
        float pulseSpeed =  0.1f;
        if (this.damage > 0) {
            pulseSpeed =  (float)(0.1 - (this.damage / 100f * 0.05f));
        }
        if (this.getTemperature() >= 6000) {
            this.setAnimation(this.brokenAnimation);
        }
        else if (this.getTemperature() < 6000 && this.getDamage() == 100) {
            this.setAnimation(this.extinguishedAnimation);
        }
        else if (this.getTemperature() >= 4000) {
            this.setAnimation(this.dangerAnimation);
        }
        else {
            this.setAnimation(normalAnimation);
        }
        this.getAnimation().setFrameDuration(pulseSpeed);

    }

    public boolean repair() {
        if (this.damage <= 0) {
            return false;
        }
        int newTemp = 2000 + 40 * (this.damage - 50);

        if  (newTemp < this.getTemperature()) {
            this.setTemperature(newTemp);
        }
        this.damage = Math.max(this.damage - 50, 0);
        updateAnimation();
        return true;
    }

    public void turnOn() {
        if (this.getDamage() >= 100) {
            return;
        }
        this.isOn = true;
        setElecticityFlowInAllDevices(true);
        updateAnimation();
    }

    public void turnOff() {
        if (this.temperature >= 6000) {
            setAnimation(this.brokenAnimation);
        }
        else {
            setAnimation(this.turnedOffAnimation);
        }
        this.isOn = false;
        setElecticityFlowInAllDevices(false);
    }

    public boolean isOn() {
        return this.isOn;
    }

    public void addDevice(EnergyConsumer consumer) {
        this.devices.add(consumer);
        if (this.isOn()) {
            consumer.setPowered(true);
        }
    }

    public void removeDevice(EnergyConsumer consumer) {
        consumer.setPowered(false);
        this.devices.remove(consumer);
    }

    private void setElecticityFlowInAllDevices(boolean bool) {
        for (EnergyConsumer c: this.devices) {
            c.setPowered(bool);
        }
    }

    public boolean extinguish() {
        if (this.damage < 100) {
            return false;
        }
        this.temperature = this.getTemperature() - 4000;
        updateAnimation();
        return true;

    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        new PerpetualReactorHeating(1).scheduleFor(this);
    }


}
