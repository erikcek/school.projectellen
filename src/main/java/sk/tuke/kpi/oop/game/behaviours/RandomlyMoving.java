package sk.tuke.kpi.oop.game.behaviours;

import sk.tuke.kpi.gamelib.Disposable;
import sk.tuke.kpi.gamelib.actions.ActionSequence;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.Wait;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.actions.Move;

import java.util.Random;


public class RandomlyMoving implements Behaviour<Movable> {

    private Disposable move;
    private int random;

    public RandomlyMoving() {
        this.move = null;
        this.random = 1;
    }

    @Override
    public void setUp(Movable actor) {
        if (actor == null) return;
        new Loop<Movable>(
            new ActionSequence<>(
                new Invoke<Movable>(() -> {
                    Random r = new Random();
                    this.random = r.nextInt(4);
                    if (this.move == null) {
//                        this.move.dispose();
                        this.move = new Move<>(Direction.getRandomDirection(), this.random).scheduleFor(actor);
                    }
                }),
                new Wait<>(this.random),
                new Invoke<>(() -> {
                    if (this.move != null) {
                        this.move.dispose();
                        this.move = null;
                    }
                })

            )
    ).scheduleFor(actor);

    }


}
