package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.Keeper;
import sk.tuke.kpi.oop.game.items.Backpack;
import sk.tuke.kpi.oop.game.items.Collectible;

public class Drop<T extends Keeper> extends AbstractAction<T> {
    @Override
    public void execute(float deltaTime) {
        Keeper keeper = this.getActor();
        if (keeper == null) {
            this.setDone(true);
            return;
        }
        Scene s = keeper.getScene();
        if (s == null) {
            this.setDone(true);
            return;
        }
        Collectible c = keeper.getBackpack().peek();
        if (c == null) return;
        s.addActor(c, keeper.getPosX(), keeper.getPosY());
        Backpack b =  keeper.getBackpack();
        b.remove(c);
        this.setDone(true);
    }
}
