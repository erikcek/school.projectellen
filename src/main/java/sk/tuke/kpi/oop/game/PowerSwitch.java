package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.graphics.Color;

public class PowerSwitch extends AbstractActor {

    private Switchable sw;

    public PowerSwitch(Switchable sw) {
        Animation a = new Animation("sprites/switch.png", 16, 16);
        setAnimation(a);
        this.sw = sw;
    }

    Switchable getDevice() {
        return this.sw;
    }

    void switchOn() {
        if (this.sw == null) {
            return;
        }
        this.sw.turnOn();
        getAnimation().setTint(Color.WHITE);
    }

    void switchOff() {
        if (this.sw == null) {
            return;
        }
        this.sw.turnOff();
        getAnimation().setTint(Color.GRAY);
    }

    public void toggle() {
        if (this.sw.isOn()) {
            this.switchOff();
        }
        else {
            this.switchOn();
        }
    }
}
