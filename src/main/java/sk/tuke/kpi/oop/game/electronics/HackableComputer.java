package sk.tuke.kpi.oop.game.electronics;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.messages.Topic;

public class HackableComputer extends AbstractActor implements Hackable {

    public static final Topic<HackableComputer> COMPUTER_HACKED = Topic.create("computer hacked", HackableComputer.class);
    private Network network;

    public HackableComputer() {
        Animation animation = new Animation("sprites/computer.png", 80, 48, 0.2f, Animation.PlayMode.LOOP_PINGPONG);
        this.setAnimation(animation);
        this.network = Network.getInstance();
    }

    @Override
    public void hack() {
        Scene s = this.getScene();
        if (s == null || this.network == null) return;
        this.network.addHackedComputer();
        s.getMessageBus().publish(HackableComputer.COMPUTER_HACKED, this);
    }

}
