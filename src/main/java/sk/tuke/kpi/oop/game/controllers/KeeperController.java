package sk.tuke.kpi.oop.game.controllers;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Input;
import sk.tuke.kpi.gamelib.KeyboardListener;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.oop.game.Keeper;
import sk.tuke.kpi.oop.game.items.Collectible;
import sk.tuke.kpi.oop.game.items.Usable;
import sk.tuke.kpi.oop.game.actions.Drop;
import sk.tuke.kpi.oop.game.actions.Shift;
import sk.tuke.kpi.oop.game.actions.Take;
import sk.tuke.kpi.oop.game.actions.Use;
import sk.tuke.kpi.oop.game.items.Backpack;


public class KeeperController<T extends Keeper> implements KeyboardListener {

    private T actor;

    public KeeperController(T actor) {
        this.actor = actor;
    }

    @Override
    public void keyPressed(@NotNull Input.Key key) {
        if (key == Input.Key.ENTER) {
            new Take<T>().scheduleFor(actor);
        }
        if (key == Input.Key.BACKSPACE) {
            new Drop<T>().scheduleFor(actor);
        }
        if (key == Input.Key.S) {
            new Shift<T>().scheduleFor(actor);
        }
        if (key == Input.Key.U) {
            this.keyU();

        }
        if (key == Input.Key.B) {
            this.keyB();
        }
    }

    private void keyB() {
        Backpack b = this.actor.getBackpack();
        if (b == null) return;
        Collectible topItem = b.peek();
        if (topItem instanceof Usable) {
            new Use<>((Usable<?>)topItem).scheduleForIntersectingWith(this.actor);
        }
//        if (! (b.peek() instanceof Usable)) return;
//        Usable<?> a = (Usable) b.getContent().get(b.getSize() - 1);
//        if (a == null ) return;
//        new Use<>(a).scheduleForIntersectingWith(this.actor);
    }

    private void keyU() {
        Scene s = this.actor.getScene();
        if (s == null) return;
        Actor us = this.intersectsWithUsable(s);
        if (us == null) return;

        new Use<>((Usable<?>) us).scheduleForIntersectingWith(this.actor);
    }

    private Actor intersectsWithUsable(Scene s) {
        return s.getActors().stream()
            .filter(this.actor::intersects)
            .filter(item -> item instanceof Usable)
            .findFirst()
            .orElse(null);
    }
}
