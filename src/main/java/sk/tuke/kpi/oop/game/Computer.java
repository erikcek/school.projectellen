package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;

public class Computer extends AbstractActor implements EnergyConsumer {

    private Animation normalAnimation;
    private Animation turnedOffanimation;
    private boolean isPowered;

    public Computer() {
        this.normalAnimation = new Animation("sprites/computer.png", 80, 48, 0.2f, Animation.PlayMode.LOOP_PINGPONG);
        this.turnedOffanimation = new Animation("sprites/computer.png", 80, 48, 0f);
        this.isPowered = false;
        setAnimation(this.turnedOffanimation);
    }

    @Override
    public void setPowered(boolean bool) {
        this.isPowered = bool;
        updateAniamtion();
    }

    public int add(int a, int b) {
        if (!this.isPowered) {
            return 0;
        }
        return a + b;
    }

    public float add(float a, float b) {
        if (!this.isPowered) {
            return 0;
        }
        return a + b;
    }

    public int sub(int a, int b) {
        if (!this.isPowered) {
            return 0;
        }
        return a - b;
    }

    public float sub(float a, float b) {
        if (!this.isPowered) {
            return 0;
        }
        return a - b;
    }

    private void updateAniamtion() {
        if (this.isPowered) {
            this.setAnimation(this.normalAnimation);
        }
        else {
            this.setAnimation(this.turnedOffanimation);
        }
    }
}
