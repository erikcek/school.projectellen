package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.Player;
import sk.tuke.kpi.gamelib.framework.actions.Loop;


public abstract class PlayerFolower extends AbstractActor {
    private int steps;
    private boolean activated;

    protected PlayerFolower(int steps) {
        this.steps = steps;
        this.activated = false;
    }

    abstract void catchAction(Player player);

    private int calculatePosition(int playerPos, int scenePos) {
        if (playerPos > scenePos) {
            return scenePos + steps;
        }
        if (playerPos < scenePos) {
            return scenePos - steps;
        }
        else {
            return scenePos;
        }
    }

    public void follow() {
        if (!this.activated) {
            return;
        }
        Scene s = this.getScene();
        Player player = (Player)s.getFirstActorByName("Player");
        this.setPosition(this.calculatePosition(player.getPosX(), this.getPosX()),
            this.calculatePosition(player.getPosY(), this.getPosY()));
        this.catchAction(player);
    };

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        new Loop<>(new Invoke<>(this::follow)).scheduleFor(this);
    }

    public void activate() {
        this.activated = true;
    }

//    public void deactivate() {
//        this.activated = false;
//    }
}
