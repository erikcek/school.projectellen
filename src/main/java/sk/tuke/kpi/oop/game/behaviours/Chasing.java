package sk.tuke.kpi.oop.game.behaviours;

import sk.tuke.kpi.gamelib.Disposable;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.actions.Move;
import sk.tuke.kpi.oop.game.characters.Ripley;

import java.util.Arrays;

public class Chasing implements Behaviour<Movable> {

    private Disposable move;

    public Chasing() {
        this.move = null;
    }

    @Override
    public void setUp(Movable actor) {
        Scene s = actor.getScene();
        if (s == null) return;
        Ripley r = (Ripley) s.getFirstActorByName("Ellen");
        if (r == null) return;
        new Loop<Movable>(new Invoke<Movable>(() -> {
            if (this.move != null) {
                this.move.dispose();
            }
            int x = r.getPosX();
            int y = r.getPosY();
            int newX;
            int newY;
            if (actor.getPosX() > x) {
                newX = - 1;
            }
            else if (actor.getPosX() < x)  {
                newX =  1;
            }
            else {
                newX = 0;
            }

            if (actor.getPosY() > y) {
                newY = -1;
            }
            else if (actor.getPosY() < y) {
                newY = 1;
            }
            else {
                newY = 0;
            }

//            final Rectangle2D.Float a = new Rectangle2D.Float(actor.getPosX(), actor.getPosY(), actor.getWidth(), actor.getHeight());

//            Actor inter = s.getActors().stream()
//                .filter(item -> item instanceof Movable)
//                .filter(item -> item != actor)
//                .filter(item -> {
//                    Rectangle2D.Float rec = new Rectangle2D.Float(
//                        item.getPosX(), item.getPosY(), item.getWidth(), item.getHeight());
//                    return rec.intersects(a);
//                })
//                .findFirst()
//                .orElse(null);

            final int tx = newX;
            final int ty = newY;
            Direction d = Arrays.stream(Direction.values())
                .filter(item -> item.getDx() == tx && item.getDy() == ty).findFirst().orElse(Direction.NONE);
            this.move = new Move<Movable>(d, 0.1f).scheduleFor(actor);

        })).scheduleFor(actor);

    }


}
