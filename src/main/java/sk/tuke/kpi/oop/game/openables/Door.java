package sk.tuke.kpi.oop.game.openables;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.map.MapTile;
import sk.tuke.kpi.gamelib.messages.Topic;
import sk.tuke.kpi.oop.game.items.Usable;



public class Door extends AbstractActor implements Openable, Usable<Actor>{
    public static final Topic<Door> DOOR_OPENED = Topic.create("door opened", Door.class);
    public static final Topic<Door> DOOR_CLOSED = Topic.create("door closed", Door.class);

     public static enum Orientation {
         HORIZONTAL,
         VERTICAL
    };

     private Orientation o;

    private Animation normalAnimation;
    private boolean isopen;

    public Door() {
        this.normalAnimation = new Animation("sprites/vdoor.png", 16, 32, 0.1f, Animation.PlayMode.ONCE_REVERSED);
        this.setAnimation(this.normalAnimation);
        this.o = Orientation.VERTICAL;
        this.isopen = false;
    }

    public Door(Orientation o ) {
        if (o == Orientation.VERTICAL) {
            this.normalAnimation = new Animation("sprites/vdoor.png", 16, 32, 0.1f, Animation.PlayMode.ONCE_REVERSED);
        }
        else if (o == Orientation.HORIZONTAL) {
            this.normalAnimation = new Animation("sprites/hdoor.png", 32, 16, 0.1f, Animation.PlayMode.ONCE_REVERSED);
        }
        this.setAnimation(this.normalAnimation);
        this.o = o;
        this.isopen = false;
    }

    public Door(String name, Orientation o) {
        super(name);

        if (o == Orientation.VERTICAL) {
            this.normalAnimation = new Animation("sprites/vdoor.png", 16, 32, 0.1f, Animation.PlayMode.ONCE_REVERSED);
        }
        else if (o == Orientation.HORIZONTAL) {
            this.normalAnimation = new Animation("sprites/hdoor.png", 32, 16, 0.1f, Animation.PlayMode.ONCE_REVERSED);
        }
        this.setAnimation(this.normalAnimation);
        this.o = o;
        this.isopen = false;
    }

    @Override
    public void open() {
        Scene s = this.getScene();
        if (s == null) {
            return;
        }
        MapTile t = s.getMap().getTile(this.floorpos(this.getPosX() / 16), this.floorpos(this.getPosY() / 16));
        MapTile t2;
        if (this.o == Orientation.VERTICAL) {
            t2 = s.getMap().getTile(this.floorpos(this.getPosX() / 16), this.floorpos(this.getPosY() / 16) + 1);
        }
        else {
            t2 = s.getMap().getTile(this.floorpos(this.getPosX() / 16) + 1, this.floorpos(this.getPosY() / 16));
        }
        t.setType(MapTile.Type.CLEAR);
        t2.setType(MapTile.Type.CLEAR);
        this.normalAnimation.stop();
        this.normalAnimation.setPlayMode(Animation.PlayMode.ONCE);
        this.normalAnimation.play();
        this.isopen = true;
        s.getMessageBus().publish(DOOR_OPENED, this);
    }

    @Override
    public void close() {
        Scene s = this.getScene();
        if (s == null) {
            return;
        }
        MapTile t = s.getMap().getTile(this.floorpos(this.getPosX() / 16), this.floorpos(this.getPosY() / 16));
        MapTile t2;
        if (this.o == Orientation.VERTICAL) {
            t2 = s.getMap().getTile(this.floorpos(this.getPosX() / 16), this.floorpos(this.getPosY() / 16) + 1);
        }
        else {
            t2 = s.getMap().getTile(this.floorpos(this.getPosX() / 16) + 1, this.floorpos(this.getPosY() / 16));
        }
        t.setType(MapTile.Type.WALL);
        t2.setType(MapTile.Type.WALL);
        this.normalAnimation.stop();
        this.normalAnimation.setPlayMode(Animation.PlayMode.ONCE_REVERSED);
        this.normalAnimation.play();
        this.isopen = false;
        s.getMessageBus().publish(DOOR_CLOSED, this);
    }

    @Override
    public boolean isOpen() {
        return this.isopen;
    }

    @Override
    public void useWith(Actor actor) {
        if (actor == null) {
            return;
        }
        if (this.isOpen()) {
            this.close();
        }
        else {
            this.open();
        }
    }

    private int floorpos(int pos) {
        return (int)Math.floor(pos);
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        this.close();
    }

    @Override
    public Class<Actor> getUsingActorClass() {
        return  Actor.class;
    }

}
