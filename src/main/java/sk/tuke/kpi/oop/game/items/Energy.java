package sk.tuke.kpi.oop.game.items;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.characters.Alive;
import sk.tuke.kpi.oop.game.characters.Health;
//import sk.tuke.kpi.oop.game.characters.Ripley;

public class Energy extends AbstractActor implements Usable<Alive> {
    public Energy() {
        Animation normalAnimation = new Animation("sprites/energy.png", 16, 16);
        this.setAnimation(normalAnimation);
    }

    @Override
    public void useWith(Alive actor) {
        if (actor == null) return;
        Scene s = this.getScene();
        if (s == null) return;
        Health h = actor.getHealth();
        if (h == null) return;
        h.restore();
        s.removeActor(this);
    }

    @Override
    public Class<Alive> getUsingActorClass() {
        return Alive.class;
    }



//    @Override
//    public Class<T> getUsingActorClass() {
//        return null;
//    }
}
