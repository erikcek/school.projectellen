package sk.tuke.kpi.oop.game.items;

import sk.tuke.kpi.gamelib.Actor;

public interface Usable<A extends Actor> {
    public void useWith(A actor);
    public Class<A> getUsingActorClass();
}
