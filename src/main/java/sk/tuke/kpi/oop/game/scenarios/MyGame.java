package sk.tuke.kpi.oop.game.scenarios;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sk.tuke.kpi.gamelib.*;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.map.ActorMapObject;
import sk.tuke.kpi.gamelib.messages.MessageBus;
import sk.tuke.kpi.oop.game.behaviours.Chasing;
import sk.tuke.kpi.oop.game.behaviours.Observing;
import sk.tuke.kpi.oop.game.characters.*;
import sk.tuke.kpi.oop.game.controllers.*;
import sk.tuke.kpi.oop.game.electronics.*;
import sk.tuke.kpi.oop.game.items.Ammo;
import sk.tuke.kpi.oop.game.items.Backpack;
import sk.tuke.kpi.oop.game.items.Energy;
import sk.tuke.kpi.oop.game.openables.Door;
import sk.tuke.kpi.oop.game.openables.LockedDoor;
import sk.tuke.kpi.oop.game.weapons.BulletBehavior;
import sk.tuke.kpi.oop.game.weapons.SuperBullet;
import sk.tuke.kpi.oop.game.weapons.SuperGun;

import java.util.ArrayList;
import java.util.List;

public class MyGame implements SceneListener {

    private static int phase = 0;
    private int messageIndex = 0;
    private String message;
    private List<String> messages;

    public MyGame() {
        this.messages = new ArrayList<String>();
        this.messages.add("Hello there, you know the drill. There is a problem with main security \nswitch for whole building." +
            "Unfortunately the switch is on the end \nof the floor and there are lots of security features that you need\n" +
            "to overcome. \nIt looks like all computer access cards have been taken by aliens.");
        this.messages.add("It looks like one of the aliens had the computer access card.\n" +
            "Take it and try to access to computer network in order to open \nhangar door. Try to \"H\"ack it");
        this.messages.add("Be careful. It looks like\nthe mothership of aliens sending here new ones\n" +
            "They realy do not want you to get to the switch.");
        this.messages.add("Oh deer. They are sending super monsters here. Take cover!");
        this.messages.add("Great! You found another computer access card.\n Go on hack the computer and open the hangar door.");
        this.messages.add("Another giant monster. Be careful.");
        this.messages.add("Watch out!");
        this.messages.add("Okay. this is starting to get messy.\nWe have teleported you a super gun. Pres \"C\" to fire super bullets.");
        this.messages.add("Okay. Now just kill the mother of them all.");
        this.messages.add("Okay. Now just kill the mother of them all.");
        this.messages.add("After you hack the computer, destroy the switch.\n The new gun should be able to do it.");
        this.messages.add("After you hack the computer, destroy the switch.\n The new gun should be able to do it.");
        this.messages.add("AGreat. You made it! now the mission is over");
        this.message = this.messages.get(this.messageIndex);
    }

    public static class Factory implements ActorFactory {
        @Override
        public @Nullable Actor create(@Nullable String type, @Nullable String name) {
            switch(name) {
                case "ellen": return new Ripley();
                case "door": return new Door(type, Door.Orientation.HORIZONTAL);
                case "alien": return MyGame.generateAlien(type);
                case "energy": return new Energy();
                case "ammo": return new Ammo();
                case "computer": return new HackableComputer();
                case "desk": return new DeskComputer();
                case "destroyable switch": return new DestroyableSwitch();
                case "accessDoor": return new LockedDoor(type, type.equals("3") ? Door.Orientation.VERTICAL : Door.Orientation.HORIZONTAL);
                default: return null;
            }
        }
    }

    public static class NextPhase implements ActorFactory {
        @Override
        public @Nullable Actor create(@Nullable String type, @Nullable String name) {
            switch(name) {
                case "alien": return MyGame.generateAlien(type);
                case "monster": return MyGame.generateMonster(type);
                case "handyMonster": return MyGame.generateHandyMonster(type);
                case "alienMother": return MyGame.generateAlienMother(type);
                case "access card": return MyGame.generateAccessCard(type);
                default: return null;
            }
        }
    }

    @Override
    public void sceneUpdating(@NotNull Scene scene) {
        Ripley r = (Ripley) scene.getFirstActorByName("Ellen");
        if (r == null) {
            return;
        }

        r.showRiplayState(scene);
        this.showMessage(scene);
        scene.follow(r);
    }

    @Override
    public void sceneInitialized(@NotNull Scene scene) {
        Ripley r = this.getRipley(scene);
        if (r == null) return;

        MovableController<Ripley> mc = new MovableController<>(r);
        ChaisingShooterConstroller<Ripley> sc = new ChaisingShooterConstroller<>(r);
        KeeperController<Ripley> kc = new KeeperController<>(r);
        HackerController<Ripley> hc = new HackerController<>(r);
        scene.getInput().registerListener(sc);
        scene.getInput().registerListener(kc);
        scene.getInput().registerListener(mc);
        scene.getInput().registerListener(hc);

        MessageBus bus = scene.getMessageBus();

        bus.subscribe(Alien.ENEMY_DIED, item -> {
            this.checkAndStartNextPhase(scene);
        });

        bus.subscribe(Monster.MONSTER_DIED, item ->
            this.checkAndStartNextPhase(scene)
        );

        bus.subscribe(HandyMonster.HANDYMONSTER_DIED, item -> {
            this.checkAndStartNextPhase(scene);
            this.giveSuperGun(r, null);
        });

        bus.subscribe(AlienMother.ALIENMOTHER_DIED, item -> {
            this.checkAndStartNextPhase(scene);
            this.giveSuperGun(r, new BulletBehavior<SuperBullet>() {
                @Override
                public void setUp(SuperBullet actor) {
                     new Loop<>(new Invoke<Actor>(() -> {
                         Actor destroyableSwitch = scene.getFirstActorByType(DestroyableSwitch.class);
                         if (destroyableSwitch != null && actor.intersects(destroyableSwitch)) {
                                 ((DestroyableSwitch) destroyableSwitch).destroy();
                                 scene.removeActor(actor);
                         }
                     })).scheduleFor(actor);
                }
            });
        });


        bus.subscribe(HackableComputer.COMPUTER_HACKED, item -> {
            this.removeComputerAccessCardFromBP(r);
            this.openNextDoor(scene);
            this.nextMessage();
        });

        bus.subscribe(DeskComputer.DESK_HACKED, item -> {
            this.removeComputerAccessCardFromBP(r);
            this.openNextDoor(scene);
        });

        bus.subscribe(DestroyableSwitch.SWITCH_DESTROYED, item -> {
            this.nextMessage();
        });
    }

    private void checkAndStartNextPhase(Scene scene) {
         Actor a = scene.getActors().stream()
            .filter(item -> item instanceof Alive)
            .filter(item -> item instanceof Enemy)
            .findFirst()
            .orElse(null);
        if (a == null) {
            this.setPhase(MyGame.phase + 1);
            this.generateActors(scene, new NextPhase());
            this.nextMessage();
        }
    }

    private void setPhase(int phase) {
        MyGame.phase = phase;
    }

    private void generateActors(Scene scene, ActorFactory factory) {
        scene.getMap();

        for (ActorMapObject actorMapObject : scene.getMap().getActorObjects()) {
            var actor = factory.create(actorMapObject.getType(), actorMapObject.getName());
            if (actor == null) continue;
            scene.addActor(actor, actorMapObject.getPosX(), actorMapObject.getPosY());
        }
    }

    private void removeComputerAccessCardFromBP(Ripley r) {
        Backpack b = r.getBackpack();
        if (b == null) return;
        b.getContent()
            .stream()
            .filter(i -> i instanceof ComputerAccessCard)
            .forEach(b::remove);
    }

    private void openNextDoor(Scene scene) {
        if (scene == null) return;
        scene.getActors().stream()
            .filter(item -> item instanceof LockedDoor)
            .forEach(item -> {
                if (MyGame.phase == 7) {
                    Network n = Network.getInstance();
                    if (n == null) return;
                    if (n.getNumberOfHackedComputers() == 4 && item.getName().equals(String.valueOf(MyGame.phase))) {
                        ((LockedDoor) item).unlock();
                    }
                }
                else if (item.getName().equals(String.valueOf(MyGame.phase))) {
                    ((LockedDoor) item).unlock();
                }
            });
    }

    private void giveSuperGun(Ripley r, BulletBehavior<? super SuperBullet> behavior) {
        r.setFirearm(new SuperGun(30, 30, behavior));
    }

    protected static Actor generateAlien(String type) {
        if (type.equals(String.valueOf(MyGame.phase))) {
            return new Alien(1 + MyGame.phase * 20, new Observing<>(
                Door.DOOR_OPENED,
                actor -> actor.getName().equals(String.valueOf(MyGame.phase)),
                new Chasing()
            ));
        }
        return null;
    }

    protected static Actor generateMonster(String type) {
        if (type.equals(String.valueOf(MyGame.phase))) {
            return new Monster(new Chasing());
        }
        return null;
    }
    protected static Actor generateHandyMonster(String type) {
        if (type.equals(String.valueOf(MyGame.phase))) {
            return new HandyMonster(new Chasing());
        }
        return null;
    }

    protected static Actor generateAlienMother(String type) {
        if (type.equals(String.valueOf(MyGame.phase))) {
            return new AlienMother(new Chasing());
        }
        return null;
    }

    protected static Actor generateAccessCard(String type) {
        if (type.equals(String.valueOf(MyGame.phase))) {
            return new ComputerAccessCard();
        }
        return null;
    }

    private Ripley getRipley(Scene scene) {
        return (Ripley) scene.getFirstActorByName("Ellen");
    }

    private void nextMessage() {
        if (this.messageIndex + 1 < this.messages.size()) {
            this.messageIndex += 1;
            this.message = this.messages.get(this.messageIndex);
        }
    }

    private void showMessage(Scene scene) {
        if (this.message != null) {
            int windowHeight = scene.getGame().getWindowSetup().getHeight();
            int yTextPos = windowHeight - GameApplication.STATUS_LINE_OFFSET;
            scene.getGame().getOverlay().drawText(message, 30, yTextPos - 50);
        }
    }
}
