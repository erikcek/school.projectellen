package sk.tuke.kpi.oop.game.electronics;

public interface Hackable {
    public void hack();
}
