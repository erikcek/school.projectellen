package sk.tuke.kpi.oop.game.items;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sk.tuke.kpi.gamelib.ActorContainer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Backpack implements ActorContainer<Collectible> {

    private int capacity;
    private String name;
    private List<Collectible> content;

    public Backpack(String name, int capacity) {
        this.name = name;
        this.capacity = capacity;
        this.content = new ArrayList<>();
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @NotNull
    @Override
    public List<Collectible> getContent() {
        return new ArrayList<>(this.content);
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getSize() {
        return this.content.size();
    }

    @Override
    public void add(@NotNull Collectible actor) {
        if (this.getSize() == this.getCapacity()) {
            throw new IllegalStateException(this.getName() + "is full");
        }
        this.content.add(actor);
    }

    @Override
    public void remove(@NotNull Collectible actor) {
        int size = this.getSize();
        if (size > 0) {
            this.content.remove(actor);
        }
    }

    @NotNull
    @Override
    public Iterator<Collectible> iterator() {
        return this.content.iterator();
    }

    @Override
    public @Nullable Collectible peek() {
        int size = this.getSize();
        if (size <= 0) {
            return null;
        }
        return this.content.get(size - 1);
    }

    @Override
    public void shift() {
        if (this.getSize() > 1) {
            Collections.rotate(this.content, 1);
        }
    }

}
