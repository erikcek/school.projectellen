package sk.tuke.kpi.oop.game.controllers;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Input;
import sk.tuke.kpi.gamelib.KeyboardListener;
import sk.tuke.kpi.oop.game.Keeper;
import sk.tuke.kpi.oop.game.actions.Use;
import sk.tuke.kpi.oop.game.items.Backpack;
import sk.tuke.kpi.oop.game.items.Collectible;
import sk.tuke.kpi.oop.game.items.Usable;


public class HackerController<T extends Keeper> implements KeyboardListener {

    private T actor;

    public HackerController(T actor) {
        this.actor = actor;
    }

    @Override
    public void keyPressed(@NotNull Input.Key key) {
        if (key == Input.Key.H) {
            Backpack b = this.actor.getBackpack();
            if (b == null) return;
            Collectible topItem = b.peek();
            if (topItem instanceof Usable) {
                new Use<>((Usable<?>)topItem).scheduleForIntersectingWith(this.actor);
            }
        }
    }
}
