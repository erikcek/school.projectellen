package sk.tuke.kpi.oop.game.electronics;

public class Network {

    private static Network instance = null;
    private int numberOfHackedComputers;

    public Network() {
        this.numberOfHackedComputers = 0;
    };

    public static Network getInstance() {
        if (instance == null) {
            Network.instance = new Network();
        }
        return Network.instance;
    }

    public void addHackedComputer() {
        this.numberOfHackedComputers += 1;
    }

    public int getNumberOfHackedComputers() {
        return numberOfHackedComputers;
    }
}
