package sk.tuke.kpi.oop.game.scenarios;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.GameApplication;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.SceneListener;
//import sk.tuke.kpi.gamelib.actions.When;
//import sk.tuke.kpi.oop.game.actions.Use;
import sk.tuke.kpi.oop.game.characters.Ripley;
import sk.tuke.kpi.oop.game.controllers.KeeperController;
import sk.tuke.kpi.oop.game.controllers.MovableController;
import sk.tuke.kpi.oop.game.items.*;

public class FirstSteps implements SceneListener {
    private Ripley r;
    @Override
    public void sceneInitialized(@NotNull Scene scene) {
        defaultScenario(scene);
    }

    void defaultScenario(@NotNull Scene scene) {
        Ripley r = new Ripley();
        r.getBackpack().shift();
        scene.addActor(r, 100, 100);

        MovableController<Ripley> mc = new MovableController<Ripley>(r);
        KeeperController<Ripley> kc = new KeeperController<>(r);
        scene.getInput().registerListener(mc);
        scene.getInput().registerListener(kc);

        Energy e = new Energy();
        //r.setEnergy(10);
        scene.addActor(e, 50,50);

        Ammo a = new Ammo();
        r.setAmmo(500);
        scene.addActor(a, 20, 20);

        Hammer h = new Hammer();
        scene.addActor(h, 10, 10);

        Wrench w = new Wrench();
        scene.addActor(w, 0, 0);

        FireExtinguisher f = new FireExtinguisher();
        scene.addActor(f, 0, 30);

//        new When<>(
//            () -> r.intersects(h),
//            new Take<Ripley>()
//        ).scheduleFor(r);

//        new When<>(
//            () -> r.intersects(e),
//            new Use<Ripley>(e)
//        ).scheduleFor(r);

//        new When<>(
//            () -> r.intersects(a),
//            new Use<Ripley>(a)
//        ).scheduleFor(r);



        this.r = r;
    }


    @Override
    public void sceneUpdating(@NotNull Scene scene) {
        if (this.r != null) {
            int windowHeight = scene.getGame().getWindowSetup().getHeight();
            int yTextPos = windowHeight - GameApplication.STATUS_LINE_OFFSET;
            scene.getGame().getOverlay().drawText(String.valueOf(r.getEnergy()), 110, yTextPos);
            scene.getGame().getOverlay().drawText(String.valueOf(r.getAmmo()), 190, yTextPos);
            scene.getGame().pushActorContainer(r.getBackpack());
        }
    }
}
