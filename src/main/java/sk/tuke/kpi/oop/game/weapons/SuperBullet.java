package sk.tuke.kpi.oop.game.weapons;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.characters.Alive;
import sk.tuke.kpi.oop.game.characters.Armed;

public class SuperBullet extends AbstractActor implements Fireable {

    private BulletBehavior<? super SuperBullet> behaviour;

    private int speed;
    private Animation normalAnimation;

    public SuperBullet(BulletBehavior<? super SuperBullet> b) {
        this.speed = 3;
        this.normalAnimation = new Animation("sprites/life.png", 16, 16);
        this.setAnimation(this.normalAnimation);
        this.behaviour = b;
    }

    @Override
    public void startedMoving(Direction direction) {
        this.normalAnimation.setRotation(direction.getAngle());
    }

    @Override
    public void collidedWithWall() {
        Scene s = this.getScene();
        if (s != null) {
            s.removeActor(this);
        }
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        if (this.behaviour != null) {
            this.behaviour.setUp(this);
        }
        else {
            new Loop<>(new Invoke<Actor>(() -> {
                Actor enemy = scene.getActors().stream()
                    .filter(item -> item instanceof Alive && ! (item instanceof Armed))
                    .filter(item -> item.intersects(this))
                    .findFirst()
                    .orElse(null);
                if (enemy != null) {
                        ((Alive)enemy).getHealth().drain(50);
                        scene.removeActor(this);
                }
            })).scheduleFor(this);
        }

    }

    @Override
    public int getSpeed() {
        return speed;
    }
}
