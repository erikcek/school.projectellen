package sk.tuke.kpi.oop.game.characters;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.ActionSequence;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.Wait;
import sk.tuke.kpi.gamelib.actions.When;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.messages.Topic;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.behaviours.Behaviour;

public class AlienMother extends AbstractActor implements Enemy, Alive, Movable {

    public static final Topic<AlienMother> ALIENMOTHER_DIED = Topic.create("enemy died", AlienMother.class);

    private Health health;
    private Behaviour<? super AlienMother> behaviour;
    private int speed;

    public AlienMother(Behaviour<? super AlienMother> b) {
        Animation normalAnimation = new Animation("sprites/mother.png", 112, 162, 0.2f);
        normalAnimation.setRotation(180);
        this.setAnimation(normalAnimation);
        this.health = new Health(100);
        this.behaviour = b;
        this.speed = 1;
        this.health.onExhaustion(this::die);
    }

    @Override
    public Health getHealth() {
        return this.health;
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        this.behaviour.setUp(this);
        scene.getActors().stream()
            .filter(item -> item instanceof Alive)
            .filter(item -> !(item instanceof Enemy))
            .forEach(item -> this.decreaseHealthAction(item));
        if (this.behaviour != null) {
            this.behaviour.setUp(this);
        }
    }

    private void decreaseHealthAction(Actor actor) {

        new Loop<>(
            new When<>(() -> this.intersects(actor),
                new ActionSequence<>(
                    new Invoke<Actor>(() -> {
                        Alive aliveActor = (Alive)actor;
                        aliveActor.getHealth().drain(100);
                    }),
                    new Wait<>(1)
                )
            )
        ).scheduleFor(this);
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    private void die() {
        Scene s = this.getScene();
        if (s == null) return;
        s.removeActor(this);
        this.dieMessage();
    }

    private void dieMessage() {
        Scene s = this.getScene();
        if (s != null) {
            s.getMessageBus().publish(AlienMother.ALIENMOTHER_DIED, this);
        }
    }
}
