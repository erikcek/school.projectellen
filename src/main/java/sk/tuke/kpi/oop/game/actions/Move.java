package sk.tuke.kpi.oop.game.actions;

import org.jetbrains.annotations.Nullable;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Action;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Movable;

public class Move<T extends Movable> implements Action<T> {

    private Direction direction;
    private float duration;
    private T actor;
    private boolean started;
    private float starteddeltaTime;
    private boolean stopped;

    public Move(Direction direction, float duration) {
        this.duration = duration;
        this.direction = direction;
        this.started = false;
        this.starteddeltaTime = 0;
        this.stopped = false;
    }

    public Move(Direction direction) {
        this.direction = direction;
        this.duration = 1;
        this.started = false;
        this.starteddeltaTime = 0;
        this.stopped = false;
    }

    @Override
    public void setActor(@Nullable T actor) {
        this.actor = actor;
    }

    @Override
    public @Nullable T getActor() {
        return this.actor;
    }

    @Override
    public boolean isDone() {
//        if (this.stopped || this.duration < this.starteddeltaTime || Math.abs(this.duration - this.starteddeltaTime) <= 1e-5) {
        if (this.stopped) {
            return true;
        }
        return false;

    }

    @Override
    public void reset() {
        this.duration = 1;
        this.started = false;
        this.starteddeltaTime = 0;
        this.stopped = false;
        //Maybe set actor to starting x a nd y, not implemetnted so far.
    }

    @Override
    public void execute(float deltaTime) {
        if (this.actor == null) {
            this.stopped = true;
            return;
        }
        this.starteddeltaTime += deltaTime;
        Scene s = this.actor.getScene();
        if (this.duration < this.starteddeltaTime || Math.abs(this.duration - this.starteddeltaTime) <= 1e-5) {
            if (this.actor != null && !this.stopped) {
                this.actor.stoppedMoving();
            }
            this.stopped = true;
        }
        else {
            if (!this.started) {
                this.actor.startedMoving(this.direction);
                this.started = true;
            }

            int speed = this.actor.getSpeed();
            int oldX = this.actor.getPosX();
            int oldY = this.actor.getPosY();
            int x = oldX + this.direction.getDx() * speed;
            int y = oldY + this.direction.getDy() * speed;
            this.actor.setPosition(x, y);
            if (s.getMap().intersectsWithWall(this.actor)) {
                this.actor.setPosition(oldX, oldY);
                actor.collidedWithWall();
            }
        }
    }

    public void stopMoving() {
        if (this.actor != null) {
            this.actor.stoppedMoving();
        }
    }

    public void stop() {
        this.stopped = true;
        stopMoving();
    }

}
