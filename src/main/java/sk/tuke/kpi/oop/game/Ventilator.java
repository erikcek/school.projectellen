package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.messages.Topic;

public class Ventilator extends AbstractActor implements Repairable {

    public static final Topic<Ventilator> VENTILATOR_REPAIRED = Topic.create("ventilator repaired", Ventilator.class);

    private Animation normalAnimation;

    public Ventilator() {
        this.normalAnimation = new Animation("sprites/ventilator.png", 32, 32, 0.1f);
        this.normalAnimation.stop();
        this.setAnimation(this.normalAnimation);
    }

    @Override
    public boolean repair() {
        this.normalAnimation.play();
        Scene s = this.getScene();
        if (s != null) {
            s.getMessageBus().publish(this.VENTILATOR_REPAIRED, this);
        }
        return true;
    }
}
