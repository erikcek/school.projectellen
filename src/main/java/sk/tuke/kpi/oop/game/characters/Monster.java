package sk.tuke.kpi.oop.game.characters;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.ActionSequence;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.Wait;
import sk.tuke.kpi.gamelib.actions.When;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.messages.Topic;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.behaviours.Behaviour;

public class Monster extends AbstractActor implements Movable, Enemy, Alive {
    public static final Topic<Monster> MONSTER_DIED = Topic.create("enemy died", Monster.class);

    private int speed;
    private Health health;
    private Behaviour<? super Monster> behaviour;
    private Animation normalAnimation;

    public Monster(Behaviour<? super Monster> behaviour) {
        this.speed = 1;
        this.health = new Health(150, 150);
        this.normalAnimation = new Animation("sprites/monster.png", 72, 128, 0.1f);
        this.setAnimation(this.normalAnimation);
        this.behaviour = behaviour;
        this.health.onExhaustion(() -> this.die());
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public Health getHealth() {
        return health;
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        this.behaviour.setUp(this);
        scene.getActors().stream()
            .filter(item -> item instanceof Alive)
            .filter(item -> !(item instanceof Enemy))
            .forEach(item -> this.decreaseHealthAction(item));
    }

    private void decreaseHealthAction(Actor actor) {

        new Loop<>(
            new When<>(() -> this.intersects(actor),
                new ActionSequence<>(
                    new Invoke<Actor>(() -> {
                        Alive aliveActor = (Alive)actor;
                        aliveActor.getHealth().drain(20);
                    }),
                    new Wait<>(1)
                )
            )
        ).scheduleFor(this);
    }

    public void startedMoving(Direction direction) {
        float angle = direction.getAngle();
        this.normalAnimation.setRotation(angle);
        this.normalAnimation.play();
        this.setAnimation(this.normalAnimation);

    }

    private void die() {
        Scene s = this.getScene();
        if (s == null) return;
        s.removeActor(this);
        this.dieMessage();
    }

    private void dieMessage() {
        Scene s = this.getScene();
        if (s != null) {
            s.getMessageBus().publish(Monster.MONSTER_DIED, this);
        }
    }
}
