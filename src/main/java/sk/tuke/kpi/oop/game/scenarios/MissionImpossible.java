package sk.tuke.kpi.oop.game.scenarios;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sk.tuke.kpi.gamelib.*;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.While;
import sk.tuke.kpi.oop.game.Locker;
import sk.tuke.kpi.oop.game.Ventilator;
import sk.tuke.kpi.oop.game.characters.Ripley;
import sk.tuke.kpi.oop.game.controllers.KeeperController;
import sk.tuke.kpi.oop.game.controllers.MovableController;
import sk.tuke.kpi.oop.game.items.Energy;
import sk.tuke.kpi.oop.game.openables.AccessCard;
import sk.tuke.kpi.oop.game.openables.LockedDoor;
import java.time.Duration;
import java.time.Instant;

public class MissionImpossible implements SceneListener {

    private Instant time;
    private Disposable energyDecrease;

    public static class Factory implements ActorFactory {
        @Override
        public @Nullable Actor create(@Nullable String type, @Nullable String name) {
            switch (name) {
                case "ellen":
                    return new Ripley();
                case "energy":
                    return new Energy();
                case "door":
                    return new LockedDoor();
                case "access card":
                    return new AccessCard();
                case "locker":
                    return new Locker();
                case "ventilator":
                    return new Ventilator();
                default:
                    return null;
            }
        }
    }

    @Override
    public void sceneInitialized(@NotNull Scene scene) {
        this.defaultScene(scene);
    }

    private void defaultScene(@NotNull Scene scene) {
//        Ripley r = (Ripley)scene.getFirstActorByType(new Class<Ripley>);
        Ripley r = (Ripley) scene.getFirstActorByName("Ellen");

        MovableController<Ripley> mc = new MovableController<Ripley>(r);
        KeeperController<Ripley> kc = new KeeperController<>(r);
        Disposable mcd = scene.getInput().registerListener(mc);
        Disposable kcd = scene.getInput().registerListener(kc);

        this.time = Instant.now();
        scene.getMessageBus().subscribe(Ripley.RIPLEY_DIED, item -> {
            mcd.dispose();
            kcd.dispose();
        });

        scene.getMessageBus().subscribe(LockedDoor.DOOR_OPENED, (item) -> {
            this.energyDecrease = new While<>(() -> r.getEnergy() > -1, new Invoke<Actor>( () -> {
                if (Duration.between(this.time, Instant.now()).toMillis() > 250) {
                    r.setEnergy(r.getEnergy() - 1);
                    this.time = Instant.now();
                }
                return;
            })).scheduleFor(r);
        });
        if (r == null) {
            return;
        }

        scene.getMessageBus().subscribe(Ventilator.VENTILATOR_REPAIRED, item ->
            this.energyDecrease.dispose()
        );

    }

    @Override
    public void sceneUpdating(@NotNull Scene scene) {
        Ripley r = (Ripley) scene.getFirstActorByName("Ellen");
        if (r == null) {
            return;
        }
        r.showRiplayState(scene);
        scene.follow(r);
    }
}
