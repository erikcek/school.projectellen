package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.ActionSequence;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.Wait;
import sk.tuke.kpi.gamelib.actions.When;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;

public class TimeBomb extends AbstractActor {

    private float time;
    private boolean isActivated;

    private Animation normal;
    private Animation activated;
    private Animation exploded;

    public TimeBomb(float time) {
        this.time = time;
        this.isActivated = false;
        this.normal = new Animation("sprites/bomb.png");
        this.activated = new Animation("sprites/bomb_activated.png", 16,16, 0.2f, Animation.PlayMode.LOOP_PINGPONG);
        this.exploded = new Animation("sprites/small_explosion.png", 16, 16, 0.2f, Animation.PlayMode.ONCE);
        setAnimation(this.normal);
    }

    public void activate() {
        this.isActivated = true;
        updateAnimation();
        new ActionSequence<>(new Wait<>(this.time), new Invoke<>(this::explode)).scheduleFor(this);
    }

    public boolean isActivated() {
        return isActivated;
    }

    protected void explode() {
        new When<>(() -> this.exploded.getCurrentFrameIndex() == 7, new Invoke<>(this::removeFromScene)).scheduleFor(this);
        setAnimation(this.exploded);
    }

    protected void removeFromScene() {
        Scene s = this.getScene();
        if (s != null) {
            s.removeActor(this);
        }
    }

    private void updateAnimation() {
        if (this.isActivated) {
            setAnimation(this.activated);
        }
        else {
            setAnimation(this.normal);
        }
    }

    public float getTime() {
        return time;
    }

    public Animation getExploded() {
        return exploded;
    }
}
