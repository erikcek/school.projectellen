package sk.tuke.kpi.oop.game.controllers;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Input;
import sk.tuke.kpi.gamelib.KeyboardListener;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.actions.Move;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MovableController<T extends Movable> implements KeyboardListener {

    private T actor;
    private Map<Input.Key, Direction> keyDirectionMap = Map.ofEntries(
        Map.entry(Input.Key.UP, Direction.NORTH),
        Map.entry(Input.Key.DOWN, Direction.SOUTH),
        Map.entry(Input.Key.LEFT, Direction.WEST),
        Map.entry(Input.Key.RIGHT, Direction.EAST)
    );

    private Set<Direction> directionSet;

    private Move<T> moveAction;
    //private Input.Key actualKey;
    private Direction direction;

    @Override
    public void keyPressed(@NotNull Input.Key key) {
        if (this.keyDirectionMap.containsKey(key)) {
            this.directionSet.add(this.keyDirectionMap.get(key));

            getDirection();
            if (this.moveAction != null) {
                this.moveAction.stop();
            }
            this.moveAction = new Move<T>(this.direction, 9999999);
            this.moveAction.scheduleFor(this.actor);
        }
    }

    public MovableController(T actor) {
        this.actor = actor;
        this.direction = Direction.NONE;
        this.directionSet = new HashSet<>();
    }

    @Override
    public void keyReleased(@NotNull Input.Key key) {
        if (this.keyDirectionMap.containsKey(key)) {
            this.directionSet.remove(this.keyDirectionMap.get(key));
            this.getDirection();
            if (this.moveAction != null) {
                this.moveAction.stop();
            }
            if (this.direction != Direction.NONE) {
                this.moveAction = new Move<>(this.direction, 9999999);
                this.moveAction.scheduleFor(this.actor);
            }
        }
    }

    public void getDirection() {
        if (this.directionSet.size() > 1){
            this.direction = this.directionSet.stream().reduce((d1, d2) -> d1.combine(d2)).orElse(Direction.NONE);
        }
        else {
            this.direction = this.directionSet.stream().findFirst().orElse(Direction.NONE);
        }
    }

}
