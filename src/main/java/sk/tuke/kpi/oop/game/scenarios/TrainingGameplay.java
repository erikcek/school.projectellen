package sk.tuke.kpi.oop.game.scenarios;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.*;
import sk.tuke.kpi.gamelib.framework.Scenario;
import sk.tuke.kpi.oop.game.*;
import sk.tuke.kpi.oop.game.items.FireExtinguisher;
import sk.tuke.kpi.oop.game.items.Hammer;
import sk.tuke.kpi.oop.game.items.Mjolnir;
import sk.tuke.kpi.oop.game.items.Wrench;

public class TrainingGameplay extends Scenario {

    @Override
    public void setupPlay(@NotNull Scene scene) {
        teleportScenario(scene);
    }

    void baseScenario(@NotNull Scene scene) {
        Reactor reactor = new Reactor();
        scene.addActor(reactor, 64, 64);
        reactor.turnOn();

        Cooler cooler = new Cooler(reactor);
        cooler.turnOff();
        scene.addActor(cooler, 194, 194);


        Hammer hammer = new Hammer();
        scene.addActor(hammer, 250, 3);

        new ActionSequence<>(
            new Wait<>(5),
            new Invoke<>(cooler::turnOn)
        ).scheduleFor(cooler);

//        new When<>(
//            () -> reactor.getTemperature() >= 3000,
//            new Invoke<>(() -> reactor.repair(hammer))
//        ).scheduleFor(reactor);
    }

    void smarCoolingScenario(@NotNull Scene scene) {
        Reactor reactor = new Reactor();
        reactor.increaseTemperature(2000);
        scene.addActor(reactor, 100, 100);

        SmartCooler sCooler_1 = new SmartCooler(reactor);
        SmartCooler sCooler_2 = new SmartCooler(reactor);
        scene.addActor(sCooler_1, 50, 250);
        scene.addActor(sCooler_2, 150, 250);
    }

    void switchScenatio(@NotNull Scene scene) {
        Reactor reactor = new Reactor();
        scene.addActor(reactor, 100, 100);

        Light l = new Light();
        scene.addActor(l, 300, 300);
        reactor.addDevice(l);

        Cooler c = new Cooler(reactor);
        scene.addActor(c, 300, 35);

        PowerSwitch pw1 = new PowerSwitch(reactor);
        scene.addActor(pw1, 200, 200);

        PowerSwitch pw2 = new PowerSwitch(l);
        scene.addActor(pw2, 150, 200);

        PowerSwitch pw3 = new PowerSwitch(c);
        scene.addActor(pw3, 200, 150);
    }

    void energyConsumersScenario(@NotNull Scene scene) {
        Reactor reactor = new Reactor();
        scene.addActor(reactor, 100, 100);

        Computer c = new Computer();
        scene.addActor(c, 100, 250);
        reactor.addDevice(c);

        Light l = new Light();
        scene.addActor(l, 300, 300);
        reactor.addDevice(l);

    }

    void extTest(@NotNull Scene scene) {
        Reactor reactor = new Reactor();
        reactor.increaseTemperature(4500);
        scene.addActor(reactor, 100, 100);

        FireExtinguisher f = new FireExtinguisher();
        scene.addActor(f, 200,200);
    }

    void repairScenario(@NotNull Scene scene) {
        Reactor reactor = new Reactor();
        reactor.increaseTemperature(2000);
        scene.addActor(reactor, 100, 100);

        Mjolnir m = new Mjolnir();
        scene.addActor(m, 200, 200);
    }

    void defectiveLightScenario(@NotNull Scene scene) {
        Reactor reactor = new Reactor();
        scene.addActor(reactor, 100, 100);

        SmartCooler sCooler_1 = new SmartCooler(reactor);
        SmartCooler sCooler_2 = new SmartCooler(reactor);
        scene.addActor(sCooler_1, 50, 250);
        scene.addActor(sCooler_2, 150, 250);

        DefectiveLight dl = new DefectiveLight();
        scene.addActor(dl, 50, 300);

        reactor.addDevice(dl);

        Wrench w = new Wrench();
        scene.addActor(w, 222,222);
    }

    void bombScenario(@NotNull Scene scene) {
        TimeBomb b = new TimeBomb(2);
        scene.addActor(b, 100,100);
    }

    void chainBombScenario(@NotNull Scene scene) {
        ChainBomb b1 = new ChainBomb(2);
        ChainBomb b2 = new ChainBomb(2);
        ChainBomb b3 = new ChainBomb(2);
        ChainBomb b4 = new ChainBomb(2);

        scene.addActor(b1, 10, 10);
        scene.addActor(b2, 59, 10);
        scene.addActor(b3, 59, 59);
        scene.addActor(b4, 79, 79);
    }

    void teleportScenario(@NotNull Scene scene) {
        Teleport t1 = new Teleport(null);
        Teleport t2 = new Teleport(t1);
        t1.setDestination(t2);

        scene.addActor(t2, 50,50);
        scene.addActor(t1, 150,350);

    }

    void helicopterScenario(@NotNull Scene scene) {
        Helicopter h = new Helicopter();
        scene.addActor(h,350,350);

        new ActionSequence<>(
            new Wait<>(5),
            new Invoke<>(h::searchAndDestroy)
        ).scheduleFor(h);
    }

}
