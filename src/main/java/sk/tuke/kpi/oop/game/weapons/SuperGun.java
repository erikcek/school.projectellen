package sk.tuke.kpi.oop.game.weapons;

public class SuperGun extends Firearm {

    private BulletBehavior<? super SuperBullet> behavior;

    public SuperGun(int ammo, int maxAmmo, BulletBehavior<? super SuperBullet> behavior) {
        super(ammo, maxAmmo);
        this.behavior = behavior;
    }

    @Override
    protected Fireable createBullet() {
        return new SuperBullet(behavior);
    }
}
