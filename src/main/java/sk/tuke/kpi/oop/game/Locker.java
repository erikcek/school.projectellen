package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.characters.Ripley;
import sk.tuke.kpi.oop.game.items.Hammer;
import sk.tuke.kpi.oop.game.items.Usable;

public class Locker extends AbstractActor implements Usable<Ripley> {

    private boolean closed;

    public Locker() {
        this.closed = true;
        Animation normal = new Animation("sprites/locker.png");
        this.setAnimation(normal);
    }

    @Override
    public void useWith(Ripley actor) {
        if (!this.closed) return;
        if (actor != null) {
            Scene s = actor.getScene();
            if (s == null) return;
            Hammer h = new Hammer();
            s.addActor(h,this.getPosX(), this.getPosY());
            this.closed = false;
        }
    }

    @Override
    public Class<Ripley> getUsingActorClass() {
        return Ripley.class;
    }
}
