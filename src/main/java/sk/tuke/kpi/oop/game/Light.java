package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;

public class Light extends AbstractActor implements Switchable, EnergyConsumer {

    private boolean electricityFlow;
    private Animation lightOff;
    private Animation lightOn;
    private boolean isOn;

    public Light() {
        this.electricityFlow = false;
        this.isOn = false;
        this.lightOff= new Animation("sprites/light_off.png");
        this.lightOn= new Animation("sprites/light_on.png");
        setAnimation(this.lightOff);
    }

    @Override
    public void setPowered(boolean bool) {
        this.electricityFlow = bool;
        this.updateAnimation();
    }

    private void updateAnimation() {
        if (this.isOn) {
            if (this.electricityFlow) {
                setAnimation(this.lightOn);
            }
            else {
                setAnimation(this.lightOff);
            }
        } else {
            setAnimation(lightOff);
        }
    }

    @Override
    public boolean isOn() {
        return isOn;
    }

    @Override
    public void turnOn() {
        this.isOn = true;
        this.updateAnimation();
    }

    @Override
    public void turnOff() {
        this.isOn = false;
        this.updateAnimation();
    }

    public void toggle() {
        if (this.isOn) {
            this.isOn = false;
        }
        else {
            this.isOn = true;
        }
        this.updateAnimation();
    }
}
