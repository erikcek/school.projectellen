package sk.tuke.kpi.oop.game.items;


import java.util.List;

public interface Storeable {
    public List<Collectible> getContent();
}
