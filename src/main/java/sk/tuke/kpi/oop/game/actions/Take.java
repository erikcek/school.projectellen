package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.Keeper;
import sk.tuke.kpi.oop.game.items.Backpack;
import sk.tuke.kpi.oop.game.items.Collectible;

public class Take<T extends Keeper> extends AbstractAction<T> {

    @Override
    public void execute(float deltaTime) {
        T actor = this.getActor();
        if (actor == null) {
            this.setDone(true);
            return;
        }
        Scene s = actor.getScene();
        if (s == null) {
            this.setDone(true);
            return;
        }
        Collectible a = (Collectible) s.getActors().stream()
            .filter(f -> f instanceof Collectible && f.intersects(this.getActor())).findFirst().orElse(null);
        if (a != null) {
            try {
                Backpack b = actor.getBackpack();
                b.add(a);
                s.removeActor(a);
            }
            catch (IllegalStateException exception) {
                s.getOverlay().drawText(exception.getMessage(), 0, 0).showFor(2);
            }
        }
        this.setDone(true);
    }
}
