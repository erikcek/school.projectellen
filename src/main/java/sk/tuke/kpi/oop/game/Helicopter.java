package sk.tuke.kpi.oop.game;
import sk.tuke.kpi.gamelib.framework.Player;
import sk.tuke.kpi.gamelib.graphics.Animation;

public class Helicopter extends PlayerFolower {

    public Helicopter() {
        super(1);
        Animation normalAnimation = new Animation("sprites/heli.png", 64, 64, 0.1f);
        this.setAnimation(normalAnimation);
    }

    public void catchAction(Player player) {
        if (player.intersects(this)) {
            player.setEnergy(player.getEnergy() - 1);
        }
    }

    public void searchAndDestroy() {
        this.activate();
    }
}
